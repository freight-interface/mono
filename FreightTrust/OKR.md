

2 DApps

Block Array Management DApp

Freight Trust DApp

OKR for BAM Dapp

Objective: Turn EDI tokens (ERC20) into either a NFT token (ERC721) OR Add Wallet Address to a Registry Contract that whitelists that address to enable it to interact with all of our DApps

Results: 
EDI becomes a utility 
EDI becomes a software license in essence
Whitelisted Address has ability to add up to 5 other addresses per license 
License expires after a certain amount of time (e.g. 12 months per license)
EDI tokens are deposited into a company owned wallet, to be recirculated to nodes as rewards
EDI tokens can be sent back to businesses as a reward as well
Registry Contract connected to our Database for User Registration

**
Questions
**

for the BAM Dapp, should we bundle a "DocuSign" App as well? 
how to manage employees for companies? 



OKR for Freight Trust DApp:

Objective: Enable the creation of Bills of Lading and Parametric Insurance Smart Contracts. Enable DocuSign like functionality through DApp

Results: 
Bills of Lading become an ERC721 NFT token 
BOL NFT tokens can become "insured" against delays or detentions (requires mobile app)
Shippers and Carriers are insured against detention / delays
Detention Factoring becomes possible (we buy the claim of detention and payout 40-50cents on the $1)
Bills of Lading can be "Traded" for Invoice Factoring (more adv. feature)


Mobile App

We can integrate with Trust Wallet (thru SDK) for adding accounts