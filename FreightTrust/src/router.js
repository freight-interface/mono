import Vue from 'vue'
import Router from 'vue-router'
import AuthPage from '@/views/Auth'
import SigninPage from '@/views/auth/Signin'
import SignupPage from '@/views/auth/Signup'
import ForgotPage from '@/views/auth/Forgot'
import ResetPage from '@/views/auth/Reset'
import DashboardPage from '@/views/Dashboard'
import CompanyCreatePage from '@/views/dashboard/Create'
import CompanyPage from '@/views/dashboard/Company'
import DashboardIndexPage from '@/views/dashboard/Index'
import CompanyEditPage from '@/views/dashboard/company/Edit'
import CompanyMembersPage from '@/views/dashboard/company/Members'
import CompanylicensePage from '@/views/dashboard/company/License'
import store from '@/store/index'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/dashboard'
    },
    {
      path: '/auth',
      name: 'AuthPage',
      component: AuthPage,
      redirect: '/signin',
      meta: { 
        public: true 
      },
      children: [
        {
          path: 'signin',
          name: 'signinpage',
          component: SigninPage,
          meta: { 
            public: true 
          }
        },
        {
          path: 'signup',
          name: 'signuppage',
          component: SignupPage,
          meta: { 
            public: true 
          }
        },
        {
          path: 'forgot',
          name: 'forgotpage',
          component: ForgotPage,
          meta: { 
            public: true 
          }
        },
        {
          path: 'reset/:reset_token',
          name: 'resetpage',
          component: ResetPage,
          meta: { 
            public: true 
          }
        }
      ]
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: DashboardPage,
      meta: { 
        public: true 
      },
      redirect: '/dashboard/',
      children: [
        {
          path: 'create',
          name: 'Company-Create',
          component: CompanyCreatePage,
          meta: { 
            public: true 
          },
        },
        {
          path: '/',
          name: 'DashboardIndex',
          component: DashboardIndexPage,
          meta: { 
            public: true 
          },

        },
        {
          path: 'company/:id',
          name: 'Company',
          component: CompanyPage,
          meta: { 
            public: true 
          },
          children: [
            {
              path: 'edit',
              name: 'Company-Edit',
              component: CompanyEditPage,
              meta: { 
                public: true 
              },
            },
            {
              path: 'members',
              name: 'Company-Members',
              component: CompanyMembersPage,
              meta: { 
                public: true 
              },
            },
            {
              path: 'license',
              name: 'Company-license',
              component: CompanylicensePage,
              meta: { 
                public: true 
              },
            }
          ]
        }
      ]
    }
  ],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  const user = store.getters.user
  if (to.meta.public) {
    if (user) {
      next('/dashboard')
    } else {
      next()
    }
  } else {
    if (user) {
      next()
    } else {
      next('/auth/signin')
    }
  }
})

export default router;