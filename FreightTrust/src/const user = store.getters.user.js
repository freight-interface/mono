const user = store.getters.user
if (to.meta.public) {
  if (user) {
    next('/dashboard')
  } else {
    next()
  }
} else {
  if (user) {
    next()
  } else {
    next('/auth/signin')
  }
}
})