import Vue from 'vue'
import Vuex from 'vuex'
import user from '@/store/modules/user'
import company from '@/store/modules/company'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersistence({
  key: 'vuex',
  storage: window.localStorage,
  reducer: state => ({
    user: state.user
  })
})

export default new Vuex.Store({
  modules: {
    user: user,
    company: company
  },
  plugins: [vuexLocalStorage.plugin]
})
