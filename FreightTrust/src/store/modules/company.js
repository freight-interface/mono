import Vue from 'vue'
import Vuex from 'vuex'
import httpService from '@/shared/httpService'

Vue.use(Vuex)

const state = {
  companies: [],
  company: null
}

const getters = {
  companies: (state) => { return state.companies },
  company: (state) => { return state.company }
}


const mutations = {
  setCompanies: function (state, companies) {
    state.companies = companies
  },
  setCompany: function (state, company) {
    state.company = company
  }
}

const actions = {
  getCompaniesList: function (context, opts) {
    return new Promise((resolve, reject) => {
      httpService.get('/company', { with: 'members' }, opts).then(resp => {

        context.commit('setCompanies', resp.data)

        resolve(resp)
      }, error => {
        reject(error.response ? error.response.data : error.message)
      })
    })
  },

  getCompany: function (context, [id, opts]) {
    return new Promise((resolve, reject) => {
      if (context.state.company && context.state.company._id === id) {
        resolve(context.state.company)
      } else {
        let companyList = context.getters.companies
        if (!companyList || !companyList.length) {
          context.dispatch('getCompaniesList')
        }
        httpService.get(`/company/${id}`, opts).then(resp => {
          context.commit('setCompany', resp)
          resolve(resp)
        }, error => {
          reject(error.response ? error.response.data : error.message)
        })
      }
    })
  },

  addOrUpdateCompany: function (context, company) {
    return new Promise((resolve, reject) => {
      delete company.members
      let request = '';
      if (!company._id) {
        request = httpService.post('/company', company);
      } else {
        request = httpService.put(`/company/${company._id}`, company);
      }
      request.then(resp => {
        if (company._id) {
          let companyList = context.getters.companies
          let index = getIndex(companyList, resp._id)
          if (index > -1) {
            resp.members = companyList[index].members
          }
          context.commit('setCompany', resp)
        }
        context.dispatch('updateCompaniesList', resp)
        resolve(resp)
      }, error => {
        reject(error.response ? error.response.data : error.message)
      })
    })
  },

  deleteCompany: function (context, id) {
    return new Promise((resolve, reject) => {
      httpService.delete(`/company/${id}`).then(resp => {
        resolve(resp)
      }, error => {
        reject(error.response ? error.response.data : error.message)
      })
    })
  },

  updateCompaniesList: function (context, company) {

    let companyList = context.getters.companies
    let index = getIndex(companyList, company._id)
    if (index > -1) {
      company.members = companyList[index].members
      companyList[index] = company
    } else {
      companyList.push(company)
    }

    context.commit('setCompanies', companyList)
  },

  updateCompanyMembersList: function (context, member) {

    let company = context.getters.company
    let members = company.members || []
    let index = getIndex(members, member._id)
    if (index > -1) {
      members[index] = member
    } else {
      members.push(member)
    }
    Vue.set(company, 'members', members)

    context.commit('setCompany', company)
  },

  addOrUpdateCompanyMember: function (context , [member, opts]) {
    return new Promise((resolve, reject) => {
      let request = '';
      if (!member._id) {
        request = httpService.post('/member', member, opts);
      } else {
        request = httpService.put(`/member/${member._id}`, member, opts);
      }
      request.then( resp => {
        context.dispatch('updateCompanyMembersList', resp)
        resolve(resp)
      }, error => {
        reject(error.response ? error.response.data : error.message)
      })
    })
  },
  deleteMemberfromStore: function (context, id) {
    let company = context.getters.company
    let members = company.members
    members = members.filter(member => member._id !== id)
    company.members = members

    context.commit('setCompany', company)
  },
  deleteCompanyMember: function (context, id) {
    return new Promise((resolve, reject) => {
      httpService.delete(`/member/${id}`).then(resp => {
        context.dispatch('deleteMemberfromStore', id)
        resolve(resp)
      }, error => {
        reject(error.response ? error.response.data : error.message)
      })
    })
  },
}

function getIndex (list, id) {
  let index = -1
  if (list && list.length) {
    index = list.findIndex(item => {
      return item._id === id
    })
  }
  return index
}

export default {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}