import Vue from 'vue'
import Vuex from 'vuex'
import httpService from '@/shared/httpService'

Vue.use(Vuex)
const state = {
  user: null,
  token: null
}
const getters = {
  token: (state) => { return state.token },
  user: (state) => { return state.user }
}
const mutations = {
  setToken: function (state, token) {
    state.token = token
  },
  setUser: function (state, user) {
    state.user = user
  }
}
const actions = {
  signup: function (context, [user, opts]) {
    return new Promise((resolve, reject) => {
      httpService.post('/register', user, opts).then(resp => {
        user = resp
        context.commit('setToken', user.access_token)
        context.commit('setUser', user)
        resolve(resp)
      }, error => {
        reject(error.response.data)
      })
    })
  },
  signin: function (context, [user, opts]) {
    return new Promise((resolve, reject) => {
      httpService.post('/login', user, opts).then(resp => {
        user = resp
        context.commit('setToken', user.access_token)
        context.commit('setUser', user)
        resolve(resp)
      }, error => {
        reject(error.response.data)
      })
    })
  },
  forgot: function (context, [user, opts]) {
    return new Promise((resolve, reject) => {
      httpService.post('/forgot', user, opts).then(resp => {
        resolve(resp)
      }, error => {
        reject(error.response.data)
      })
    })
  },
  reset: function (context, [user, opts]) {
    return new Promise((resolve, reject) => {
      httpService.post('/reset', user, opts).then(resp => {
        resolve(resp)
      }, error => {
        reject(error.response.data)
      })
    })
  },
  logoutUser: function (context) {
    context.commit('setToken', null)
    context.commit('setUser', null)
  }
}
export default {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}
