/* This service handle all comunication with sales contract */
import web3Service from '@/services/web3Service'
import abi from '@/contracts/sales.json'

const salesService = {
  salesContract: null,
  isProcessing: false,
  init: function () {
    let salesContractAddress = process.env.VUE_APP_FREIGHT_TRUST_SALE_CONTRACT
    this.salesContract = new web3Service.web3.eth.Contract( abi, salesContractAddress )
  },
  fetchLicenseRate: function () {
      return new Promise((resolve, reject) => {
        // this.salesContract.methods.getEDIPerToken()
        //   .call((error, resp) => {
        //     if (!resp) {
        //       reject(resp)
        //     }
        //     resolve(resp)
        //   })
        let salesContractAddress = process.env.VUE_APP_FREIGHT_TRUST_SALE_CONTRACT
        let txOption = {
          to: salesContractAddress,
          data: web3Service.web3.eth.abi.encodeFunctionCall({
            name: 'getEDIPerToken',
            type: 'function',
            inputs: []
          },
          [])
        }
        web3Service.web3.eth.call(txOption).then(resp => {
          if (!resp) {
            reject(resp)
          }
          let rateAry = web3Service.web3.utils.toBN(resp).toString()
          resolve(rateAry)
        })
      })
  },
  approveSpender: function (spenderAddress, amount) {
    return new Promise((resolve, reject) => {
      let aryTokenAddress = process.env.VUE_APP_EDI_TOKEN_ADDRESS
      let owner = web3Service.accounts[0]
      let txOption = {
        from: owner,
        to: aryTokenAddress,
        data: web3Service.web3.eth.abi.encodeFunctionCall({
          name: 'approve',
          type: 'function',
          inputs: [{
            type: 'address',
            name: 'spender'
          },{
            type: 'uint256',
            name: 'tokens'
          }]
        }, [spenderAddress, amount])
      }
      this.isProcessing = true
      web3Service.web3.eth.sendTransaction(txOption).then(resp => {
        this.isProcessing = false
        if (!resp) {
          reject(resp)
        }
        // window.bus.$emit('allowance-success')
        resolve(resp)
      }).catch(error => {
        this.isProcessing = false
        reject(error.message)
      })
    })
  },
  applyLicense: function (companyId, whitelist, buyer) {
    return new Promise ((resolve, reject) => {
      this.isProcessing = true
      this.salesContract.methods.purchase(companyId, whitelist, buyer).send({ from: buyer }).then((receipt) => {
        if (!receipt) {
          reject(receipt)
        }
        let license_id
        if (receipt.events) {
          let keys = Object.keys(receipt.events)
          if (keys && keys.length) {
            let rawData = receipt.events[keys[1]].raw.data // in sales contract "license issued" event is 2nd thats why event index 1
            license_id = web3Service.web3.utils.toBN(rawData).toString()
          }
        }
        this.isProcessing = false
        // window.bus.$emit('license-generated')
        resolve(license_id)
      }).catch(error => {
        this.isProcessing = false
        reject(error.message)
      })
    })
  },
  renewLicense: function (licenseId) {
    return new Promise((resolve, reject)=> {
      let owner = web3Service.accounts[0]
      this.isProcessing = true
      this.salesContract.methods.renew(licenseId).call({ from: owner }).then((resp) => {
        if (!resp) {
          reject(resp)
        }
        this.isProcessing = false
        resolve(resp)
      }).catch(error => {
        this.isProcessing = false
        console.log(error)
      })
    })
  },
  addWhitelistedMembers: function (license_id, addresses) {
    return new Promise((resolve, reject) => {
      let address = web3Service.accounts[0]

      this.salesContract.methods.addLicenseWhitelistedAddressBulk(license_id, addresses)
      .send({from: address}).then((receipt) => {
        resolve(receipt)
      }).catch(error => {
        reject(error)
      })
    })
  },
  removeWhitelistedMembers: function (license_id, addresses) {
    return new Promise((resolve, reject) => {
      let address = web3Service.accounts[0]

      this.salesContract.methods.removeLicenseWhitelistedAddressBulk(license_id, addresses)
      .send({from: address}).then((receipt) => {
        resolve(receipt)
      }).catch(error => {
        reject(error)
      })
    })
  },
  checkRemainingAllowance: function () {
    /* To check allowance of using EDI tokens from owner to sales contract */
    return new Promise((resolve, reject)=> {
      let aryTokenAddress = process.env.VUE_APP_EDI_TOKEN_ADDRESS
      let owner = web3Service.accounts[0]
      let spender = process.env.VUE_APP_FREIGHT_TRUST_SALE_CONTRACT
      let txOption = {
        to: aryTokenAddress,
        data: web3Service.web3.eth.abi.encodeFunctionCall({
          name: 'allowance',
          type: 'function',
          inputs: [{
            type: 'address',
            name: 'tokenOwner'
          },{
            type: 'address',
            name: 'spender'
          }]
        },
        [owner, spender])
      }
      web3Service.web3.eth.call(txOption).then(resp => {
        if (!resp) {
          reject(resp)
        }
        let remainingAllowance = web3Service.web3.utils.toBN(resp).toString()
        resolve(remainingAllowance)
      })
    })
  }
}

export default salesService