import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'

import Navbar from '@/components/Navbar'

import mixin from '@/shared/mixins'
import toEtherFilter from '@/shared/toEtherFilter';
import toEDIFilter from '@/shared/toEDIFilter';
import toDateFilter from '@/shared/toDateFilter';
import truncateFilter from '@/shared/truncateFilter';

import '@/scss/theme.scss'

Vue.use(ElementUI, {locale})

Vue.mixin(mixin)
Vue.filter('toEther', toEtherFilter)
Vue.filter('toEDI', toEDIFilter)
Vue.filter('toDate', toDateFilter)
Vue.filter('truncate', truncateFilter)

Vue.component('navbar', Navbar)

Vue.config.productionTip = false

window.bus = new Vue()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
