import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { FCM } from '@ionic-native/fcm';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { BillsPageModule } from '../pages/bills/bills.module';

import { CompaniesPage } from '../pages/companies/companies';
import { LoginPage } from '../pages/login/login';
import { BillDetailComponent } from '../components/bill-detail/bill-detail';

import { APIProvider } from '../providers/api';
import { UserProvider } from '../providers/user';
import { Web3Provider } from '../providers/web3';
import { SessionProvider } from '../providers/session';
import { BillsProvider } from '../providers/bills';
import { UtilityProvider } from '../providers/utility';
import { CompanyProvider } from '../providers/company';
import { NotificationProvider } from '../providers/notification';

import { PublicPageModule } from '../pages/public/public.module';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CompaniesPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicPageModule.forChild(PublicPageModule),
    HttpClientModule,
    BillsPageModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CompaniesPage,
    LoginPage,
    BillDetailComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FCM,
    APIProvider,
    UserProvider,
    Web3Provider,
    SessionProvider,
    BillsProvider,
    UtilityProvider,
    CompanyProvider,
    NotificationProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
