import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { BillsPage } from '../pages/bills/bills';
import { AddBillPage } from '../pages/bills/add/add';
import { LoginPage } from '../pages/login/login';
import { CompaniesPage } from '../pages/companies/companies';
import { HomePage } from '../pages/home/home';
import { Web3Provider } from '../providers/web3';
import { UserProvider } from '../providers/user';
import { NotificationProvider } from '../providers/notification';

import { PublicPage } from '../pages/public/public';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  @ViewChild(Nav) nav: Nav;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    public web3: Web3Provider,
    private user: UserProvider,
    private notification: NotificationProvider,
    splashScreen: SplashScreen) {
    this.init();
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  init() {

    let ctx = this;
    this.user.getPassword().then((password) => {
      // let publicPage = ctx.nav.getActive().name;
      // console.log(publicPage);
      // let isPublicPage = location && location.href && location.href.indexOf('public') > -1 ? true : false;

      if (password) {
        this.user.setPassword(password);
        this.web3
          .init()
          .subscribe((initialized) => {
            this.rootPage = CompaniesPage;
          });

          // this.notification.receivedNotification({
          //   wasTapped: true,
          //   bill_id: '5b4dc13b55a0582372c6ffa4',
          //   company_id: '5b4c38e52941e9181e45ebc4'
          // })

      } else {
        this.rootPage = LoginPage;
      }
    });

  }
}

