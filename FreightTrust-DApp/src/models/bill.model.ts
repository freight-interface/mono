import { Item } from './item.model';

export class Bill {
  _id: string;
  shipper: any;
  consignee: any;
	delivery_data: string;
	quote_no: string;
  status: string;
  items: Item[];
  carrier_signature: string;
  shipper_signature: string;
  carrier_address: string;
  company_id: string;

  constructor() {
  	this.shipper = {
  		name: '',
		  phone: '',
			address: '',
			city: '',
			state: '',
			zip: ''
  	}

  	this.consignee = {
  		name: '',
		  phone: '',
			address: '',
			city: '',
			state: '',
			zip: ''
  	}
  }
}