export class Item {
  id: number;
  name: string;
  detail: string;
  quantity: number;
  weight: number;
}