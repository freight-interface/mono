export class Company {
	_id: string;
	name: string;
	website: string;
	phone: string;
	email: string;
	user_id: string;
	is_owner: boolean;
	license_id: string;
	license_valid: boolean;
}