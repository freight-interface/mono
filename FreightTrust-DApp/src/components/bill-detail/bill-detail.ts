import { Component, Input } from '@angular/core';
import { Events, ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BillDetailComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'bill-detail',
  templateUrl: 'bill-detail.html',
  styleUrls: ['/components/bill-detail/bill-details.scss']
})
export class BillDetailComponent {

  text: string;
  @Input('bill') bill: any = {
    shipper: {
      
    },
    consignee: {
      
    },
    items: []
  };
  @Input('allowEdit') allowEdit: boolean = false;
  
  constructor(
    public events: Events,
    public viewCtrl: ViewController,
    public navParams: NavParams
  ) {
    console.log('Hello BillDetailComponent Component');
    console.log(this.bill)
  }

  ionViewDidLoad() {
    if (this.navParams.data._id) {
      this.bill = this.navParams.data
      this.allowEdit = false
    }
  }

  editShipper () {
    this.events.publish('bill:edit', 'shipper');
  }

  editConsignee () {
    this.events.publish('bill:edit', 'consignee');
  }

  editItems () {
    this.events.publish('bill:edit', 'items');
  }

  dismiss() {
    this.viewCtrl.dismiss()
  }
}
