import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { BillDetailComponent } from './bill-detail/bill-detail';
@NgModule({
	declarations: [BillDetailComponent],
	imports: [IonicModule],
	exports: [BillDetailComponent]
})
export class ComponentsModule {}
