import { Component} from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { Item } from '../../../models/item.model';

@Component({
  selector: 'page-add-item',
  templateUrl: 'item.html'
})

export class AddItemPage {
  item: Item;

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    this.item = new Item();
  }

  ngAfterViewInit() {
    if (this.navParams.data.id) {
      this.item = this.navParams.data;
    }
  }

  save() {
    this.close(this.item);
  }

  close(data:any = null) {
    if (data) {
      delete data.component;
      delete data.opts;
    }
    this.viewCtrl.dismiss(data);
  }
}