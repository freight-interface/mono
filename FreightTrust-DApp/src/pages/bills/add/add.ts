import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NavParams, NavController, ModalController, Slides } from 'ionic-angular';
import { BillsProvider } from '../../../providers/bills';
import { CompanyProvider } from '../../../providers/company';
import { UtilityProvider } from '../../../providers/utility';
import { Web3Provider } from '../../../providers/web3';

import { Bill } from '../../../models/bill.model';
import { Item } from '../../../models/item.model';
import { AddItemPage } from '../item/item';
import { Events } from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { BillDetailComponent } from '../../../components/bill-detail/bill-detail';

@Component({
  selector: 'page-add-bill',
	templateUrl: 'add.html'
})

export class AddBillPage implements OnInit {
  @ViewChild('billSteps') slides: Slides;
  @ViewChild('carrierSignaturePad') carrierSignaturePad: SignaturePad;
  @ViewChild('shipperSignaturePad') shipperSignaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 2,
    'canvasWidth': 400,
    'canvasHeight': 200,
    'backgroundColor': '#f6fbff',
    'penColor': '#666a73'
  };

  current_state: string;
  current_step: number = 0;
  bill:Bill;
  shipperForm: FormGroup;
  consigneeForm: FormGroup;
  shipper_sign: boolean = false;
  
  constructor(
    public navParams: NavParams, 
    public navCtrl: NavController, 
    public modalCtrl: ModalController, 
    public billProvider: BillsProvider,
    public companyProvider: CompanyProvider,
    public utilityProvider: UtilityProvider,
    public events: Events,
    public web3: Web3Provider
  ) {
    this.current_state = 'shipper';
    this.bill = new Bill();
  }

  ngOnInit() {
    this.initValidations('shipperForm');
    this.initValidations('consigneeForm');

    if (this.navParams.data && this.navParams.data.company_id) {
      this.companyProvider.getCompanies().subscribe((companies) => {
        let company = companies.find((next_company) => { return next_company._id === this.navParams.data.company_id });
        this.companyProvider.setCompany(company);
        this.afterBillInit()
      })
    }
    
    if (this.navParams.data && this.navParams.data.bill_id) {
      this.billProvider.getBill(this.navParams.data.bill_id).subscribe(resp => {
        this.bill = resp
        this.afterBillInit()
      })
    }
  }
 
  ngAfterViewInit() {

    this.slides.lockSwipes(true);

    if (this.navParams.data._id) {
      this.bill = this.navParams.data;
      this.afterBillInit();
    }

    this.events.subscribe('bill:edit', (state) => {
      console.log('bill:edit', state);
      this.setCurrentState(state);
    });
  }

  afterBillInit() {
    if (this.bill && this.bill.carrier_signature) {
      this.carrierSignaturePad.fromDataURL(this.bill.carrier_signature);
    }
    if (this.bill && this.bill.status == 'shipper_signed') {
      this.shipper_sign = true;
      setTimeout(() => {
        this.setCurrentState('confirm');
        this.shipperSignaturePad.fromDataURL(this.bill.shipper_signature);
      }, 1000)
    }
  }

  initValidations(form) {
    this[form] = new FormGroup({
      name: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
      state: new FormControl('', [Validators.required]),
      zip: new FormControl('', [Validators.required]),
    })
  }

  setCurrentState(current_state) {
    this.current_state = current_state;
    switch(current_state) {
      case 'shipper':
        this.current_step = 0
        break;
      case 'consignee':
        this.current_step = 1
        break;
      case 'items':
        this.current_step = 2
        break;
      case 'confirm':
        this.current_step = 3
        break;
    }
    this.slides.lockSwipes(false)
    this.slides.slideTo(this.current_step)
    this.slides.lockSwipes(true)
  }

  next() {
    let form = null;
    if (this.current_step == 0) {
      form = 'shipperForm'
    } else if (this.current_step == 1) {
      form = 'consigneeForm'
    }

    if (!form || this[form].valid) {
      switch(this.current_step) {
        case 0:
          this.setCurrentState('consignee');
          break;
        case 1:
          this.setCurrentState('items');
          break;
        case 2:
          this.setCurrentState('confirm');
          break;
      }
    } else {
      this.validateFields(form)
    }
  }

  back() {
    switch(this.current_step) {
      case 0:
        this.navCtrl.pop();
        break;
      case 1:
        this.setCurrentState('shipper');
        break;
      case 2:
        this.setCurrentState('consignee');
        break;
      case 3:
        this.setCurrentState('items');
        break;
    }

    // this.current_step = this.current_step - 1;
  }

  addNewItem(item: Item = null) {
    this.bill.items = this.bill.items || []
    if (!item) {
      item = new Item();
      item.id = this.bill.items.length + 1
    }

    let itemModal = this.modalCtrl.create(AddItemPage, item);
    itemModal.onDidDismiss(data => {
      if (data) {
        let index = this.bill.items.findIndex(next_item => { return next_item.id === item.id })
        if (index < 0) {
          this.bill.items.push(item)
        } else {
          this.bill.items[index] = item
        }
      }
    })
    itemModal.present();
  }

  deleteItem(item: Item) {
    this.utilityProvider.confirmDelete().subscribe((confirm:boolean) => {
      if (confirm) {
        let index = this.bill.items.findIndex(next_item => { return next_item.id === item.id })
        if (index > -1) {
          this.bill.items.splice(index, 1)
        }
      }
    })
  }

  onSubmit(state) {
    let form = null
    switch(state) {
      case 0:
        form = 'shipperForm'
        break;
      case 1:
        form = 'consigneeForm'
        break;
    }

    if (this[form].valid) {
      this.next()
    } else {
      this.validateFields(form)
    }
  }

  validateFields(form) {
    let keys = Object.keys(this[form].controls).forEach(field => {
      const control = this[form].get(field);
      control.markAsTouched({onlySelf: true})
    })
  }

  saveBill() {
    let company = this.companyProvider.getCompany();
    this.bill.company_id = company._id;
    this.billProvider.addOrEditBill(this.bill).subscribe(resp => {
      this.utilityProvider.showToast('Bill updated successfully');
      this.navCtrl.pop();
    })
  }

  didTapViewBill() {
    let itemModal = this.modalCtrl.create(BillDetailComponent, this.bill);
    itemModal.onDidDismiss(data => {
    
    });
    itemModal.present();
  }

  confirm() {
    console.log(this.bill);
    if (this.bill && this.bill.shipper_signature && this.bill.carrier_signature) {
      this.bill.status = "approved";
    }
    else if (this.bill && this.bill.carrier_signature) {
      this.bill.status = "carrier_signed";
    } else {
      this.bill.status = "pending";
    }
    this.bill.carrier_address = this.web3.getDefaultAccount();
    let company = this.companyProvider.getCompany();
    this.bill.company_id = company._id;
    this.billProvider.addOrEditBill(this.bill).subscribe(resp => {
      this.saveBillInContract(resp)
    })
  }

  saveBillInContract(bill: Bill) {
    this.billProvider.saveBillToBlockchain(bill).subscribe(resp => {
      this.utilityProvider.showToast('Bill updated successfully');
      this.navCtrl.pop();
    })
  }

  drawComplete(signed_by): void {
    if (signed_by == 'carrier_signature') {
      this.bill[signed_by] = this.carrierSignaturePad.toDataURL('image/jpeg', 0.5);
    } else if (signed_by == 'shipper_signature') {
      this.bill[signed_by] = this.shipperSignaturePad.toDataURL('image/jpeg', 0.5);
    }
  }

  clear(signed_by): void {
    if (signed_by == 'carrier_signature') {
      this.carrierSignaturePad.clear();  
    } else if (signed_by == 'shipper_signature') {
      this.shipperSignaturePad.clear();
    }
    this.bill[signed_by] = '';
  }
}
