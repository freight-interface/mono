import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { AddBillPage } from './add/add';
import { BillsProvider } from '../../providers/bills';
import { CompanyProvider } from '../../providers/company';
import { Bill } from '../../models/bill.model';
import { Company } from '../../models/company.models';
import { BillDetailComponent } from '../../components/bill-detail/bill-detail';
import { UtilityProvider } from '../../providers/utility';
import { ENV } from '@app/env'

/**
 * Generated class for the BillsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-bills',
  templateUrl: 'bills.html',
  styleUrls: ['/pages/bills/bills.scss']
})
export class BillsPage implements OnInit {

  bills: Bill[] = [];
  company: Company = new Company();
  isLicenseValid: boolean = false;
  bills_filter: string = '';
  API_URL: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public billsProvider: BillsProvider,
    public companyProvider: CompanyProvider,
    public utility: UtilityProvider) {
      this.init();
      this.API_URL = ENV.API_BASE_URL;
  }

  ngOnInit() {
    if (this.navParams.data && this.navParams.data.company_id) {
      this.companyProvider.getCompanies().subscribe((companies) => {
        let company = companies.find((next_company) => { return next_company._id === this.navParams.data.company_id });
        this.companyProvider.setCompany(company);
        this.init();
      })
    } else {
      this.init();
    }
  }

  ionViewDidEnter() {
    // this.loadBills();
  }

  init () {
    this.company = this.companyProvider.getCompany();


    if (this.company && this.company._id) {
      this.loadBills();
      this.companyProvider.verifyCompanyLicense(this.company._id).subscribe(isValid => {
        console.log("License id: ", isValid);
        // isValid = 0;
        this.isLicenseValid = !!isValid;
      }, error => {
        console.log("Error: ", error);
        this.isLicenseValid = false;
      })
    } 
  }

  setFilter(status) {
    this.bills_filter = status;
    if (this.bills_filter.length) {
      this.loadBills ({status: this.bills_filter})
    } else {
      this.loadBills();
    }
  }

  loadBills(params:any = {}){
    Object.assign(params, {company_id: this.company._id});
    this.billsProvider.listBills(params).subscribe((bills) => {
      this.bills = bills;
    });
  }

  didTapNewBill (bill: Bill = null) {
    if (!this.isLicenseValid) {
      this.utility.showToast("Your company license has been expired. Ask company owner to renew license.");
      return;
    }
    if (bill && bill.status == 'approved') {
      this.utility.showToast("Bill has been approved by shipper, so now you cannot update it.");
      return;
    }
    this.navCtrl.push(AddBillPage, bill);
  }

  didTapRemoveBill (bill: Bill = null) {
    this.utility.confirmDelete().subscribe((confirm:boolean) => {
      if (confirm) {
        this.billsProvider.deleteBill(bill._id).subscribe((resp) => {
          let index = this.bills.findIndex(next_item => { return next_item._id === bill._id });
          if (index > -1) {
            this.bills.splice(index, 1)
          }
        });
      }
    });
  }

  didTapBillDetail (bill) {
    let itemModal = this.modalCtrl.create(BillDetailComponent, bill);
    itemModal.onDidDismiss(data => {
  
    });
    itemModal.present();
  }
}
