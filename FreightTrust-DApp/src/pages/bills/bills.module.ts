import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillsPage } from './bills';
import { AddBillPage } from './add/add';
import { AddItemPage } from './item/item';
import { ComponentsModule } from '../../components/components.module';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  declarations: [
    BillsPage,
    AddBillPage,
    AddItemPage
  ],
  imports: [
    ComponentsModule,
    SignaturePadModule,
    IonicPageModule.forChild(BillsPage),
  ],
  entryComponents: [
  	AddBillPage,
  	AddItemPage
  ]
})
export class BillsPageModule {}
