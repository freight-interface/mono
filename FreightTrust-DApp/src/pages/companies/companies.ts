import { Component, ViewEncapsulation } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BillsPage } from '../bills/bills';
import { CompanyProvider } from '../../providers/company';
import { Company } from '../../models/company.models';
import { Web3Provider } from '../../providers/web3';
import { APIProvider } from '../../providers/api';
import { ENV } from '@app/env'


@Component({
  selector: 'page-companies',
  styleUrls: ['/pages/companies/companies.scss'],
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'companies.html'
})
export class CompaniesPage {
  public API_URL = '';
  public companies: Company[];
  constructor(public navCtrl: NavController,
    public companyProvider: CompanyProvider,
    public api: APIProvider,
    public web3: Web3Provider) {
    // this.companies = this.companyProvider.getCompanies();
    this.init();
    this.API_URL = ENV.API_BASE_URL;
  }

  init() {
    this.companyProvider.fetchCompaniesFromBlockchain().subscribe(companyIds => {
      if (companyIds && companyIds.length) {
        this.companyProvider.fetchCompanies(companyIds).subscribe(companies => {
          this.companies = companies;
        }, error => {
          console.log("Error: ", error);
        })
      }
    });
  }

  didTapCompany (company) {
    this.companyProvider.setCompany(company);
    this.navCtrl.push(BillsPage)
  }

  getimage (location) {
    return !location ? 'assets/imgs/FreightTrust-logo-09.png' : `${this.api.getBaseUrl()}${location}`;
  }

}
