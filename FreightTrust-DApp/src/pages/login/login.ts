import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { CompaniesPage } from '../companies/companies';
import { UserProvider } from '../../providers/user';
import { Web3Provider } from '../../providers/web3';

@Component({
	selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['/pages/login/login.scss']
})

export class LoginPage {
  password: string = "";
  privateKey: string = "";
  hasWallet: boolean = false;
  constructor (
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    private web3: Web3Provider,
    private user: UserProvider) {
      this.hasWallet = this.web3.hasWallet();
  }

  continue() {
    this.user.setPassword(this.password);
    this.web3
      .init()
      .subscribe((initialized) => {
        if (!this.web3.hasWallet()) {
          this.web3.importFromPrivateKey(this.privateKey);
          this.navCtrl.setRoot(CompaniesPage);
        }
        else if (this.web3.loadWallet()) {
          this.navCtrl.setRoot(CompaniesPage);
        } else {
          const toast = this.toastCtrl.create({
            message: 'Incorrect password',
            duration: 3000
          })
          toast.present();
        }
      });
  }
}