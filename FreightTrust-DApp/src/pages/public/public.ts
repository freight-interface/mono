import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, IonicPage, NavParams } from 'ionic-angular';
import { BillsProvider } from '../../providers/bills';
import { CompanyProvider } from '../../providers/company';
import { UtilityProvider } from '../../providers/utility';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Bill } from '../../models/bill.model';
import { ENV } from '@app/env'

@IonicPage({
  name: 'public',
  segment: 'public/:bill_id'
})
@Component({
  selector: 'page-public',
  templateUrl: 'public.html',
  styleUrls: ['/pages/public/public.scss']
})
export class PublicPage implements OnInit{
  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  private signaturePadOptions: Object = {
    'minWidth': 2,
    'canvasWidth': 320,
    'canvasHeight': 120,
    'backgroundColor': '#f6fbff',
    'penColor': '#666a73'
  };
  billId: string = '';
  bill:Bill;
  signed: boolean = false;
  API_URL: string = '';
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public billsProvider: BillsProvider,
    public companyProvider: CompanyProvider,
    public utilityProvider: UtilityProvider) {
    this.billId = this.navParams.get('bill_id');
    console.log("NavParams: ", this.billId);
    this.bill = new Bill();
    this.API_URL = this.API_URL = ENV.API_BASE_URL;
  }

  ngOnInit() {
    this.init();
  }

  ngAfterViewInit() {
    console.log("aosjdklasd");
    console.log(this.bill);
    // if (this.bill && this.bill.shipper_signature) {
    //   this.signaturePad.fromDataURL(this.bill.shipper_signature);
    // }
  }

  init() {
    this.billsProvider.getBill(this.billId).subscribe(resp => {
      if (resp && resp._id) {
        this.bill = resp;
        console.log(this.bill);
        // if (this.bill && this.bill.shipper_signature) {
        //   this.signaturePad.fromDataURL(this.bill.shipper_signature);
        // }
      }
    }, error => {
      console.log("Error");
      console.log(error);
    });
  }

  confirm() {
    console.log(this.bill);
    this.bill.status = "shipper_signed";
    this.billsProvider.addOrEditBill(this.bill).subscribe(resp => {
      this.utilityProvider.showToast('Bill updated successfully');
      // this.navCtrl.pop();
    })
  }

  drawComplete(): void {
    console.log("Draw complete");
    this.signed = true;
    this.bill.shipper_signature = this.signaturePad.toDataURL('image/jpeg', 0.5);

    console.log(this.bill.shipper_signature);
  }

  clear(): void {
    this.signaturePad.clear();
    this.bill.shipper_signature = '';
  }

  ionViewCanLeave(): boolean{
    return false;
  }

}
