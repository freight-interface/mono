import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PublicPage } from './public';
import { ComponentsModule } from '../../components/components.module';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  declarations: [
    PublicPage
  ],
  imports: [
    ComponentsModule,
    SignaturePadModule,
    IonicPageModule.forChild(PublicPage),
  ],
  entryComponents: [
    PublicPage
  ]
})
export class PublicPageModule {}
