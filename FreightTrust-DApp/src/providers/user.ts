import { Injectable } from '@angular/core';
import { SessionProvider } from './session';
import { EventEmitter } from '@angular/core';

@Injectable()
export class UserProvider {

  currentAddress;
  addressChange: EventEmitter<any> = new EventEmitter();
  password: string = '';

  constructor(private session: SessionProvider) {
    this.init();
  }
  
  async init() {
    this.currentAddress = await this.getDefaultAddress();
  }

  async getPassword() {
    return await this.session.getAsync('password');
  }

  async getDefaultAddress() {
    return await this.session.getAsync('defaultAddress');
  }

  async setPassword(password) {
    this.password = password;
    return await this.session.setAsync('password', password);
  }

  setDefaultAddress(address: string) {
    this.currentAddress = address;
    this.session.set('defaultAddress',address);
    this.addressChange.emit(address);
  }

  addressChangeListener(){
    return this.addressChange;
  }

  logout() {
    this.session.clear();
    this.session.remove('defaultAddress');
    this.session.remove('password');
  }
}
