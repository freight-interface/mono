import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ENV } from '@app/env'

@Injectable()
export class APIProvider {
  base_url;
  headers: HttpHeaders;

  constructor(public http: HttpClient) {
    this.headers = new HttpHeaders().set('Content-Type', 'application/json');
    this.base_url = ENV.API_BASE_URL
  }

  setBaseUrl(base_url: string) {
    this.base_url = base_url;
  }

  getBaseUrl(){
    return this.base_url;
  }

  addHeader(name, value){
    this.headers = this.headers.append(name, value);
  }
  
  get(resource){
    let url = this.base_url + resource; 
    return this.http.get(url, {
      headers: this.headers
    });
  };

  post(resource, body){
    let url = this.base_url + resource;
    return this.http.post(url, body, {
      headers: this.headers
    });
  };

  update(resource, body){
    let url = this.base_url + resource; 
    return this.http.put(url, body, {
      headers: this.headers
    });
  };

  delete(resource, id){
    let url = this.base_url + resource + '/' + id; 
    return this.http.delete(url, {
      headers: this.headers
    });
  }
}