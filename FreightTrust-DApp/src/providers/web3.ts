import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Buffer } from 'buffer';
import { UserProvider } from './user';
import tx from 'ethereumjs-tx';
import Web3 from 'web3'
import { ENV } from '@app/env'

@Injectable()
export class Web3Provider {
  web3;
  initialized;
  userSecret; //This is the password but will later convert it into the Encrypted private key. 

  constructor(private user: UserProvider) {
    
  }

  init(): Observable<boolean>{
    return Observable.create(observer => {
      // Do not initialize web3 on each init call
      if (this.initialized) {
        return observer.next(true);
      }

      this.web3 = new Web3(ENV.PROVIDER_URL);
      this.loadWallet();
      this.initialized = true;
      observer.next(true);
      observer.complete();
    });
  }

  getWeb3Object() {
    return this.web3;
  }

  signTransaction(rawTx){
    let account = this.getAccountFromAddress(this.user.currentAddress);
    let privateKey = new Buffer(account.privateKey.substr(2), 'hex');
    let transaction = new tx(rawTx);
    transaction.sign(privateKey);
    let serializedTx = transaction.serialize();
    return '0x' + serializedTx.toString('hex');
  }

  call (address, fn, args, resp) {
    return Observable.create(observer => {
      let txOptions : any = this.generateCall(address, fn, args, resp);
      this.web3.eth.call(txOptions).then(data => {
        console.log(data);
        // data = this.decodeArrayOfStrings(data);
        // data = this.web3.eth.abi.decodeParameters(resp, data);
        // Decode data where you need it and how you need it
        observer.next(data);
        observer.complete();
      })
      .catch(error => {
        observer.error(error);
      })
    });
  }

  getTransactionCount(address = ''){
    address = address || this.user.currentAddress;
    return Observable.create(observer => {
      this
        .web3.eth.getTransactionCount(address)
        .then((count) => {
          observer.next(count.toString());
          observer.complete();
        })
        .catch((err) => {
          console.error(err);
          observer.complete();
        })
    });
  }

  async getBalance(contractAddress, address) {
    if (contractAddress) {
      let txOptions: any = {
        gas: this.web3.utils.toHex("210000"),
        gasPrice: this.web3.utils.toHex("3000000000"),
        to: contractAddress,
        data: this.web3.eth.abi.encodeFunctionCall({
            name: 'balanceOf',
            type: 'function',
            inputs: [{
                type: 'address',
                name: 'address'
            }]
        }, [address])
      }
      let resp = await this.web3.eth.call(txOptions); 
      let balanceInWei = this.web3.utils.toBN(resp).toString(); 
      return balanceInWei / 1.0e18; // Should be dynamic based on decimals
    }
    if (!contractAddress && address) {
      let balanceInWei = await this.web3.eth.getBalance(address);
      return balanceInWei / 1.0e18;
    }
  }

  hasWallet (): boolean {
    let wallets = window.localStorage.getItem("web3js_wallet");
    return wallets && JSON.parse(wallets).length > 0;
  }

  saveWallet(): boolean{
    this.userSecret = this.user.password;
    if (this.userSecret) {
      try {
        return this.web3.eth.accounts.wallet.save(this.userSecret);
      } catch(e) {
        console.log(e);
      }
    }
    return false;
  }

  loadWallet(): boolean{
    this.userSecret = this.user.password;
    if (this.userSecret) {
      try {
        const wallet = this.web3.eth.accounts.wallet.load(this.userSecret);
        this.setDefaultAccount(this.user.currentAddress);
        return true;
      } catch(e) {
        console.log(e);
      }
    }
    return false;
  }

  generateCall (address, fn, args, resp) {
    let txOptions : any = {
      to: address
    };
    let fnCall = {
      name: fn,
      type: 'function',
      inputs: []
    };
    let values = args.map(arg => arg.value);
    args.forEach(arg => {
      fnCall.inputs.push({type: arg.type});
    });

    txOptions.data = this.web3.eth.abi.encodeFunctionCall(fnCall, values);
    return txOptions;
  }

  setDefaultAccount(address: string) {
    this.web3.eth.defaultAccount = address;
    this.user.setDefaultAddress(address)
  }

  importFromPrivateKey(private_key){ 
    private_key = private_key.trim();
    if (private_key.indexOf('0x') !== 0) {
      private_key = '0x' + private_key; 
    }
    let account = this.web3.eth.accounts.privateKeyToAccount(private_key)
    this.web3.eth.accounts.wallet.add(account);
    
    this.setDefaultAccount(account.address)
    this.saveWallet();
    return account;
  }

  getDefaultAccount () {
    return this.web3.eth.defaultAccount;
  }

  getAccountFromAddress(address) {
    let accounts = this.getAddresses();
    return accounts.find(wallet => wallet.address === address)
  }

  getAddresses(){
    let accounts = []
    for (let i = 0; i < this.web3.eth.accounts.wallet.length; i++) {
      accounts.push(this.web3.eth.accounts.wallet[i])
    }
    return accounts;
  }

  isDefaultAccount(address) {
    return this.web3.eth.defaultAccount === address
  }

  getCurrentBalance(contractAddress = ''){
    return this.getBalance(contractAddress, this.web3.eth.defaultAccount);
  }

  decodeArrayOfStrings(hexResp) {
    const INDEX_OF_LENGTH = 1;
    let binary = hexResp.substr(2);
    let arr = binary.match(/.{1,64}/g);
    let list = [];
    if (arr && arr.length >= INDEX_OF_LENGTH) {
      let length = this.web3.utils.hexToNumber(`0x${arr[INDEX_OF_LENGTH]}`);
      if (!length) {
        return list;
      }
      let respDataSet = arr.slice(-(length * 2));
      respDataSet.forEach((data, index) => {
        if (index % 2 != 0) { 
          let str = this.web3.utils.hexToUtf8(`0x${data}`);
          list.push(str);
        }
      })
      return list;
    }
  }
}
