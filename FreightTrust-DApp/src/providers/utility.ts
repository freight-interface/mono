import { Injectable } from '@angular/core';
import { ToastController, AlertController, LoadingController } from 'ionic-angular';
import { Observable } from 'rxjs';

@Injectable()
export class UtilityProvider {
    loader: any = null;
    constructor(
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        public loadingController: LoadingController
    ) {}

    showToast(msg) {
        const toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
    }

    confirmDelete() {
      return new Observable(observer => {
          const prompt = this.alertCtrl.create({
        title: 'Confirm Delete',
        message: "Are you sure you want to delete?",
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              observer.next(false)
              observer.complete()
            }
          },
          {
            text: 'OK',
            handler: data => {
              observer.next(true)
              observer.complete()
            }
          }
        ]
      });
      prompt.present();
      })
    }

    presentLoading() {
      if (!this.loader){
        this.loader = this.loadingController.create({
          content: "Please wait..."
        });
        this.loader.onDidDismiss(() => {
          this.loader = null;
         });
        this.loader.present();
      }
    }

    dismissLoading() {
      if (this.loader) {
        this.loader.dismiss();
        this.loader = null;
      }
    }
}