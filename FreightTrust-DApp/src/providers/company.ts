import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Company } from '../models/company.models';
import { APIProvider } from './api';
import { Web3Provider } from './web3';
import { ENV } from '@app/env'

import { UtilityProvider } from './utility';

@Injectable()
export class CompanyProvider {
  companies: Company[];
  company: Company;

  constructor(private storage: Storage,
    public api: APIProvider,
    public web3: Web3Provider,
    public utility: UtilityProvider) {
  }

  getCompanies(): Observable<Company[]> {
    return Observable.create(observer => {
      if (this.companies && this.companies.length) {
        observer.next(this.companies)
        observer.complete()
      }

      setInterval(() => {
        if (this.companies && this.companies.length) {
          observer.next(this.companies)
          observer.complete()
        }
      }, 1000)
    })
    // return this.companies;
  }

  setCompany(company: Company){
    this.company = company;
  }

  getCompany() {
    return this.company;
  }

  fetchCompanies(companyIds): Observable<Company[]> {

    // fetch companies from DB
    let queryString = companyIds.join(',');
    return this.api.get(`/carriercompanies?ids=${queryString}`)
      .map(resp => <Company[]>resp)
      .do(resp => {
        if (resp) {
          this.companies = <Company[]>resp;
        }
      });
  }

  fetchCompaniesFromBlockchain() {
    return Observable.create( observer => {
      this.utility.presentLoading();
      let address = ENV.TOKEN_ADDRESS; // token/license contract address
      let functionName = "getCompanyIdByAddress";
      console.log("Default Address: ", this.web3.getDefaultAccount());
      let args = [{
        type: 'address',
        value: this.web3.getDefaultAccount()
      }];
      this.web3.call(address, functionName, args, ["string[]"]).subscribe(resp => {
        console.log('Resp: ', resp);
        resp = this.web3.decodeArrayOfStrings(resp);
        this.utility.dismissLoading();
        observer.next(resp);
        observer.complete();
        
      }, error => {
        console.log("Error: ", error);
      });
    } );
  }

  fetchlicenseId(companyId) {
    return Observable.create(observer => {
      let address = ENV.TOKEN_ADDRESS; // token/license contract address
      let functionName = "getLicenseIdByCompanyId";
      console.log("Default Address: ", this.web3.getDefaultAccount());
      let args = [{
        type: 'string',
        value: companyId
      }];
      var resp = ["string"];
      this.web3.call(address, functionName, args, resp).subscribe(licenseId => {
        console.log('Company license: ', licenseId);
        observer.next(this.web3.web3.utils.hexToNumber(licenseId));
        observer.complete();
      }, error => {
        console.log("Company don't have license");
        console.log(error);
      });
    })
  }

  verifyLicense(licenseId) {
    return Observable.create(observer => {
      let address = ENV.TOKEN_ADDRESS; // token/license contract address
      let functionName = "isValidLicense";
      console.log("Default Address: ", this.web3.getDefaultAccount());
      let args = [{
        type: 'uint256',
        value: licenseId
      }];
      var resp = ["bool"];
      this.web3.call(address, functionName, args, resp).subscribe(isValid => {
        console.log('License valid: ', isValid);
        // console.log()
        observer.next(this.web3.web3.utils.hexToNumber(isValid));
        observer.complete();
      }, error => {
        console.log("Company don't have license");
        console.log(error);
      });
    })
  }

  verifyCompanyLicense(companyId){
    return Observable.create(observer => {
      this.utility.presentLoading();
      this.fetchlicenseId(companyId).subscribe(licenseId => {
        this.verifyLicense(licenseId).subscribe(isValid => {
          this.utility.dismissLoading();
          observer.next(isValid);
          observer.complete();
        })
      })
    });
  }

  protected handleError(error: Response) {
      console.error(error);        
      var errorMessage = null;

      if (error.json() && error.json().errors) {
          errorMessage = error.json().errors[0]; 
      } else {
          errorMessage = 'Server Error';
      }

      return Observable.throw(error);
  }
}