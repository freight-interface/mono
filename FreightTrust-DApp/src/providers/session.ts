import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class SessionProvider {
  constructor(private storage: Storage) {}
  
  get(key){
    return this.storage.get(key);
  }

  async getAsync(key) {
    var value = await this.storage.get(key);
    return value;
  }

  set(key, value){
    return this.storage.set(key, value);
  }

  async setAsync(key, value){
    return await this.storage.set(key,value);
  }

  remove(key){
    return this.storage.remove(key);
  }

  async removeAsync(key){
    return await this.storage.remove(key);
  }

  clear(){
    return this.storage.clear();
  }
}