import { Injectable } from '@angular/core';
import { SessionProvider } from './session';
import { APIProvider } from './api';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Bill } from '../models/bill.model';
import { ENV } from '@app/env'

import { Web3Provider } from './web3';
import { UtilityProvider } from './utility';

@Injectable()
export class BillsProvider {

  bill: any = {
    toAddress: '',
    fromAddress: '',
    shipperName: '',
    carrierName: '',
    id: '1234',
    deliveryDate: '',
    status: '',
    items: [{
      name: '',
      value: '',
      weight: '',
      quantity: ''
    }]
  };
  constructor(
    public api: APIProvider,
    public utility: UtilityProvider,
    public web3: Web3Provider
  ) {
    let api_url = ENV.API_BASE_URL;
    this.api.setBaseUrl(api_url);
  }

  listBills (opts: any = null) {
    let params = null
    if(opts) {
      params = ''
      let keys = Object.keys(opts);
      for(var i= 0; i < keys.length; i++) {
        if (i==0) {
          params += '?'
        } else {
          params += '&'
        }
        params += keys[i] + '=' + opts[keys[i]];
      }
    }
    return Observable.create(observer => {
      let url = '/bill'
      if (opts) {
        url += params
      }
      this.api.get(url).subscribe((resp:any) => {
        observer.next(resp.data);
        observer.complete();
      })
    });
  }

  addOrEditBill (bill: Bill) {
    return Observable.create(observer => {
      let request = null;
      if (!bill._id) {
        request = this.api.post('/bill', bill);
      } else {
        request = this.api.update(`/bill/${bill._id}`, bill);
      }

      request.subscribe(resp => {
        observer.next(resp);
        observer.complete();
      }, error => {
        observer.error();
      })
    })
  }

  saveBillToBlockchain(bill: Bill) {
    return Observable.create( observer => {
      this.utility.presentLoading();
      const address = ENV.BOL_ADDRESS; // bol token contract address
      const functionName = "createBill";

      const web3 = this.web3.getWeb3Object();
      const args = [
        {
          "name": "quoteId",
          "type": "string",
        },
        {
          "name": "_companyId",
          "type": "string",
        }
      ];

      let fnCall = {
        name: functionName,
        type: 'function',
        inputs: args
      };
      const data = web3.eth.abi.encodeFunctionCall(fnCall, [
        `quote-${bill._id}`,
        bill.company_id
      ]);

      const transaction : any = {
        to: address,
        data: data,
        from: this.web3.getDefaultAccount(),
        gas: web3.utils.toHex("6000000"),
        gasPrice: web3.utils.toHex("3000000000")
      };

      this.web3.getTransactionCount().subscribe(count => {
        transaction.nonce = web3.utils.toHex(count);
        const sign = this.web3.signTransaction(transaction);
        web3.eth.sendSignedTransaction(sign)
          .then((reciept) => {
            console.log('RECEIPT', reciept)
            observer.next(reciept);
            observer.complete();
            this.utility.dismissLoading();
          })
          .catch(error => {
            console.error(error)
            observer.error(error);
            this.utility.dismissLoading();
          })
      });
      // web3.eth.sendTransaction().then(resp => {
      //   resp = this.web3.decodeArrayOfStrings(resp);
      //   console.log('Resp: ', resp);
      //   this.utility.dismissLoading();
      //   observer.next(resp);
      //   observer.complete();
        
      // }, error => {
      //   console.log("Error: ", error);
      // });
    });
  }

  getBill (id: string) {
    return Observable.create( observer => {
      this.api.get(`/bill/${id}?with=company`).subscribe(resp => {
        observer.next(resp);
        observer.complete();
      } , error => {
        observer.error();
      });
    });
  }

  deleteBill (id: string) {
    return Observable.create(observer => {
      this.api.delete('/bill', id).subscribe((resp:any) => {
        observer.next(resp.data);
        observer.complete();
      })
    });
  }
}
