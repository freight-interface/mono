import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { App, Platform , AlertController} from 'ionic-angular';
import { FCM } from '@ionic-native/fcm';
import { ENV } from '@app/env'
import { SessionProvider } from './session';
import { APIProvider } from './api';
import { UserProvider } from './user';

import { CompaniesPage } from '../pages/companies/companies'; 
import { BillsPage } from '../pages/bills/bills';
import { AddBillPage } from '../pages/bills/add/add';

@Injectable()
export class NotificationProvider {

  globalTopics: string = 'all';
  token: string = '';
  notificationListener:EventEmitter<any> = new EventEmitter();

  constructor(
    private fcm: FCM, 
    private api: APIProvider,
    private user: UserProvider,
    private app: App,
    private platform: Platform,
    public alertCtrl: AlertController) {
    if (this.platform.is('cordova')) {
      this.init();
    } else {
      console.log("Notification service not initialized");
    }

    this.user.addressChangeListener().subscribe(address => {
      this.updateToken();
    });
  }

  init() {
    this.fcm.subscribeToTopic(this.globalTopics);
    this.fcm.getToken().then(token => {
      this.updateToken(token);
    });
    this.fcm.onNotification().subscribe(data => {
      this.receivedNotification(data);
    });
    this.fcm.onTokenRefresh().subscribe(token => {
      this.updateToken(token);
    });
  }

  receivedNotification(data) {
    // alert('message received')
    if(data.wasTapped) {
      this.handleNotification({bill_id: data.bill_id, company_id: data.company_id})
      console.info("Received in background");
    } else {
      console.info("Received in foreground");
      const confirm = this.alertCtrl.create({
        message: `Bill ${data.bill_id} is signed by shipper and needs approval.`,
        buttons: [
          {
            text: 'OK',
            handler: () => {
              this.handleNotification({bill_id: data.bill_id, company_id: data.company_id})
            }
          }
        ]        
      })
      confirm.present();
    };
  }

  handleNotification(data) {
    setTimeout(() => {
      let nav = this.app.getActiveNav();
      nav.setRoot(CompaniesPage);
      nav.insertPages(1, [{
          page: BillsPage,
          params: {company_id: data.company_id}
        }, {
          page: AddBillPage,
          params: data
        }
      ])
    }, 500)
  }

  getBGNotificationListener() {
    return this.notificationListener;
  }

  updateToken (token = '') {
    if (token) {
      this.token = token;
    }
    const address = this.user.currentAddress;
    if (address && this.token) {
      this.api.post('/notification', {
        address: address,
        token: token
      }).subscribe(resp => {
        console.log(resp)
      }, error => {
        console.log(error);
      });
    }
  }


}
