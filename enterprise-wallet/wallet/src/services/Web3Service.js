import Web3 from 'web3'

const web3Service = {
  web3: null,
  accounts: [],
  network: 'Unknown',
  init: function () {
    this.web3 = new Web3(window.web3.currentProvider)
    this.accounts = []
    this.checkCurrentNetwork()
    // this.openMetamaskPopup()
  },
  getAccounts: function () {
    return new Promise((resolve, reject) => {
      this.web3.eth.getAccounts((error, accounts) => {
        if (error) {
          reject(error)
        }
        this.accounts = accounts
        resolve(accounts[0] || null)
      })
    })
  },
  checkCurrentNetwork: function () {
    window.web3.version.getNetwork((error, network) => {
      if (!error) {
        switch (network) {
          case '1':
            this.network = 'Main Network'
            break
          case '3':
            this.network = 'Ropsten Test Network'
            break
          case '4':
            this.network = 'Rinkeby Test Network'
            break
          case '42':
            this.network = 'Kovan Test Network'
            break
          default:
            this.network = 'Unknown'
            break
        }
      }
    })
  },
  getBalance: function (address) {
    return new Promise((resolve, reject) => {
      this.web3.eth.getBalance(address, (error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  getEstimatedGas: function (byteCode) {
    console.log(byteCode)
    return new Promise((resolve, reject) => {
      this.web3.eth.estimateGas({data: byteCode}, (error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  getGasPrice: function () {
    return new Promise((resolve, reject) => {
      this.web3.eth.getGasPrice((error, resp) => {
        if (error) {
          reject(error)
        }
        console.log(resp)
        resolve(resp)
      })
    })
  },
  getTransactionsCount: function (address) {
    return new Promise((resolve, reject) => {
      this.web3.eth.getTransactionCount(address, (error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  getTransactionReceipt: function (txHash) {
    console.log(txHash)
    return new Promise((resolve, reject) => {
      this.web3.eth.getTransactionReceipt(txHash, (error, resp) => {
        console.log(resp)
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  sendTransaction: function (transaction) {
    return new Promise((resolve, reject) => {
      this.openMetamaskPopup()
      this.web3.eth.sendTransaction(transaction, (error, resp) => {
        if (error) {
          reject(error)
        }
        this.closeMetamaskPopup()
        resolve(resp)
      })
    })
  },
  getContractInstance: function (contract, from) {
    let instance = new this.web3.eth.Contract(JSON.parse(contract.abi), contract.address, {'from': from})
    return instance
  },
  getContractMethods: function (abi) {
    abi = JSON.parse(abi)
    let methods = abi.filter(item => { return item.type === 'function' })
    console.log(methods)
    return methods
  },
  deployContract: function (from, contractData) {
    return new Promise((resolve, reject) => {
      var contractInstance = window.web3.eth.contract(JSON.parse(contractData.abi))
      console.log(contractInstance)
      contractInstance.new({
        from: from,
        data: contractData.byte_code,
        gas: contractData.gas_limit
      }, (error, resp) => {
        if (error) {
          reject(error)
        }
        resolve(resp)
      })
    })
  },
  callContractMethod: function (contractData, method, fromAddr) {
    let contract = new this.web3.eth.Contract(JSON.parse(contractData.abi), contractData.address)

    return new Promise((resolve, reject) => {
      let params = method.inputs.map(value => value.type === 'unint256' ? parseInt(value.value) : value.value)
      console.log(params)
      contract.methods[method.name](...params).call({from: fromAddr}, (error, result) => {
        if (error) {
          console.log(error)
          reject(error)
        }
        console.log(result)
        resolve(result)
      })
    })
  },
  sendToElectron: function (message, params = {}) {
    if (window.chrome && window.chrome.ipcRenderer) {
      window.chrome.ipcRenderer.send(message, params)
    }
  },
  openMetamaskPopup: function () {
    this.sendToElectron('open-metamask-popup')
  },
  closeMetamaskPopup: function () {
    this.sendToElectron('close-metamask-popup')
  },
  openMetamaskNotification: function () {
    this.sendToElectron('open-metamask-notification')
  },
  closeMetamaskNotification: function () {
    this.sendToElectron('close-metamask-notification')
  },
  openLink: function (link) {
    if (window.chrome && window.chrome.ipcRenderer) {
      window.chrome.ipcRenderer.send('open-link', link)
    } else {
      window.open(link, '_blank')
    }
  },
  saveData: function (filename, data) {
    this.sendToElectron('save-data', {filename: filename, content: data})
  },
  readData: function (filename) {
    return new Promise((resolve, reject) => {
      this.sendToElectron('read-data', {filename: filename})
      if (window.chrome && window.chrome.ipcRenderer) {
        window.chrome.ipcRenderer.on('after-read-data', (event, data) => {
          resolve(data)
        })
      }
    })
  },
  sendEther: function (contractFunction) {
    this.web3.eth.sendTransaction({
      to: '0x8f6c0c887F7CAF7D512C964eA2a3e668D94C5304',
      value: '1000000000000'
    }, (err, res) => {
      if (err) this.closeMetamaskNotification()
      if (res) this.closeMetamaskNotification()
    })

    setTimeout(() => {
      this.openMetamaskNotification()
    }, 500)
  }
}

export default web3Service
