import httpService from './HttpService'

export default {
  etherscan_api_url: process.env.ETHERSCAN_API,
  gas_api_url: process.env.GAS_API,
  network_api_url: process.env.NET_API,

  eth_btc: 0,
  eth_usd: 0,
  highest_block: 0,
  hash_rate: 0,
  uncle_rate: 0,
  tps: 0,
  gas_price: 0,
  gas_price_safelow: 0,
  gas_median_wait: 0,

  init: function () {
    // this.getCurrencyRates()
    // this.getBestBlock()
    // this.getHashRate()
    this.getNetworkStats()
    this.getGasStats()
  },

  getTransactions: function (address) {
    let params = {
      module: 'account',
      action: 'txlist',
      address: address,
      startblock: 0,
      endblock: 99999999,
      page: 1,
      offset: 10,
      sort: 'desc'
    }

    return new Promise((resolve, reject) => {
      httpService.get(this.etherscan_api_url, params).then(resp => {
        resolve(resp.result)
      }, error => {
        reject(error)
        console.log('Error', error)
      })
    })
  },

  getCurrencyRates: function () {
    let params = {
      module: 'stats',
      action: 'ethprice'
    }

    httpService.get(this.etherscan_api_url, params).then(resp => {
      if (resp && resp.result) {
        this.eth_btc = resp.result.ethbtc
        this.eth_usd = resp.result.ethusd
        console.log(this.eth_btc, this.eth_usd)
      }
    }, error => {
      console.log('Error', error)
    })
  },

  getBestBlock: function () {
    window.web3.eth.getSyncing((error, resp) => {
      console.log('highest block', resp, error)
      if (!error) {
        if (resp && resp.hightestBlock) {
          this.highest_block = resp.hightestBlock
        }
      }
    })
  },

  getHashRate: function () {
    window.web3.eth.getHashrate((error, resp) => {
      if (!error) {
        this.hash_rate = resp
      }
    })
  },

  getNetworkStats: function () {
    httpService.get(this.network_api_url).then(result => {
      if (result && result.currentStats) {
        this.tps = result.currentStats.tps || 0
        this.tps = this.tps.toFixed(2)

        this.uncle_rate = result.currentStats.uncle_rate
        this.uncle_rate = this.uncle_rate.toFixed(2)

        this.hash_rate = result.currentStats.hashrate / result.currentStats.difficulty
        this.hash_rate = this.hash_rate.toFixed(2)

        this.highest_block = result.blocks[0].number

        this.eth_btc = result.currentStats.price_btc
        this.eth_usd = result.currentStats.price_usd
      }
    })
  },

  getGasStats: function () {
    httpService.get(this.gas_api_url).then(result => {
      if (result) {
        this.gas_price = result.average / 10
        this.gas_price_safelow = result.safelow_calc / 10
        this.gas_median_wait = (result.avgWait * 60).toFixed(2)
      }
    })
  }
}
