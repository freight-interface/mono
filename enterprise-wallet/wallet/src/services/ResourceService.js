import httpService from './HttpService'

const resourceService = {
  api_url: process.env.API_URL,

  application: [],
  address: [],
  contract: [],

  getAll: function (resource, params = {}, opts = undefined) {
    let url = this.api_url + `/${resource}`

    return new Promise((resolve, reject) => {
      httpService.get(url, params, opts).then((resp) => {
        this[resource] = resp.data
        resolve(resp)
      }).catch(error => {
        reject(error.message)
      })
    })
  },

  addResource: function (resource, resourceData, opts = undefined) {
    let url = this.api_url + `/${resource}`

    return new Promise((resolve, reject) => {
      httpService.post(url, resourceData, opts).then(resp => {
        this[resource].push(resp)
        resolve(resp)
      }).catch(error => {
        reject(error.message)
      })
    })
  },

  updateResource: function (resource, resourceData, opts = undefined) {
    let url = this.api_url + `/${resource}/${resourceData._id}`
    return new Promise((resolve, reject) => {
      httpService.put(url, resourceData, opts).then(resp => {
        if (resp) {
          let index = this[resource].findIndex(res => { return resp._id === resourceData._id })
          if (index > -1) {
            this[resource][index] = resp
          }
        }
        resolve(resp)
      }).catch(error => {
        reject(error.message)
      })
    })
  },

  deleteResource: function (resource, resourceData, opts = undefined) {
    let url = this.api_url + `/${resource}/${resourceData._id}`

    return new Promise((resolve, reject) => {
      httpService.delete(url, opts).then(resp => {
        if (resp) {
          let index = this[resource].findIndex(res => { return res._id === resourceData._id })
          if (index > -1) {
            this[resource] = this[resource].filter((res) => { return res._id !== resourceData._id })
          }
        }
        resolve(resp)
      }).catch(error => {
        reject(error.message)
      })
    })
  }
}

export default resourceService
