import axios from 'axios'
import { Loading } from 'element-ui'

let spinnerOptions = {
  show_loading: false,
  fullscreen: true,
  target: 'document.body'
}
let loadingInstance = null
let loadingInstanceCount = 0

axios.interceptors.response.use((response) => {
  loadingInstanceCount = loadingInstanceCount > 0 ? (loadingInstanceCount - 1) : 0
  if (loadingInstance && loadingInstanceCount === 0) {
    loadingInstance.close()
  }
  return Promise.resolve(response)
}, error => {
  loadingInstanceCount = loadingInstanceCount > 0 ? (loadingInstanceCount - 1) : 0
  if (loadingInstance && loadingInstanceCount === 0) {
    loadingInstance.close()
  }

  switch (error.response.status) {
    case 401:
      window.vue.showError('Invalid credentials. Please try again!')
      break
    case 403:
      window.vue.showError('You are unauthorized to perform this action.')
      break
    case 422:
      var errors = error.response.data
      var errorKeys = Object.keys(errors)
      for (var i = 0; i < errorKeys.length; i++) {
        var texts = errors[ errorKeys[i] ]
        if (Array.isArray(texts)) {
          for (var j = 0; j < texts.length; j++) {
            window.vue.showError(texts[j])
          }
        }
      }
      break
    case 500:
      window.vue.showError('Oops, something went wrong. Please try again later.')
      break
    default:
      break
  }
  return Promise.reject(error)
})

const httpService = {
  token: null,

  get: function (url, params = {}, opts = spinnerOptions) {
    if (opts.show_loading) {
      loadingInstanceCount++
      loadingInstance = Loading.service(opts)
    }

    let config = this.mergeHeaders(url)
    config.params = params

    return axios.get(url, config).then(resp => resp.data)
  },

  post: function (url, data, opts = spinnerOptions) {
    if (opts.show_loading) {
      loadingInstanceCount++
      loadingInstance = Loading.service(opts)
    }

    let config = this.mergeHeaders(url)
    return axios.post(url, data, config).then(resp => resp.data)
  },

  put: function (url, data, opts = spinnerOptions) {
    if (opts.show_loading) {
      loadingInstanceCount++
      loadingInstance = Loading.service(opts)
    }

    let config = this.mergeHeaders(url)
    return axios.put(url, data, config).then(resp => resp.data)
  },

  delete: function (url, opts = spinnerOptions) {
    if (opts.show_loading) {
      loadingInstanceCount++
      loadingInstance = Loading.service(opts)
    }

    let config = this.mergeHeaders(url)
    return axios.delete(url, config).then(resp => resp.data)
  },

  mergeHeaders: function (url) {
    let config = {}
    config.headers = {
      'Content-Type': 'application/json'
    }

    console.log(this.isLocalAPI(url))
    if (this.token && this.isLocalAPI(url)) {
      config.headers.Authorization = `Basic ${this.token}`
    }
    return config
  },

  isLocalAPI: function (url) {
    // checks if the api call is for local api to use user token
    let apiUrl = process.env.API_URL
    return url.indexOf(apiUrl) > -1
  },

  setToken: function (token) {
    this.token = token
  },

  removeToken: function () {
    this.token = null
  }
}

export default httpService
