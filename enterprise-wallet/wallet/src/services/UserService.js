import httpService from './HttpService'

export default {
  api_url: process.env.API_URL + '/auth',

  token: null,
  user: null,

  init: function () {
    let token = localStorage.getItem('token')
    let user = localStorage.getItem('user')

    if (token) {
      this.token = token
      httpService.setToken(this.token)

      this.refreshUser()
    }

    if (user) {
      this.user = JSON.parse(user)
      httpService.setToken(this.user.access_token)
    }
  },

  register: function (user, opts = undefined) {
    let url = this.api_url + '/register'
    return new Promise((resolve, reject) => {
      httpService.post(url, user, opts).then((user) => {
        this.saveUser(user)
        resolve(user)
      }).catch(error => {
        reject(error.message)
      })
    })
  },

  login: function (user, opts = undefined) {
    let url = this.api_url + '/login'
    return new Promise((resolve, reject) => {
      httpService.post(url, user, opts).then((user) => {
        this.saveUser(user)
        resolve(user)
      },
      error => {
        reject(error.message)
      })
    })
  },

  updateUser: function (user, opts = undefined) {
    let url = this.api_url + '/update-user'
    return new Promise((resolve, reject) => {
      httpService.post(url, user, opts).then((resp) => {
        if (resp && resp.user) {
          this.saveUser(resp.user)
        }
        resolve(resp)
      },
      error => {
        reject(error.message)
      })
    })
  },

  refreshUser: function () {
    let url = this.api_url + '/refresh-user'
    httpService.get(url).then((user) => {
      this.saveUser(user)
    })
  },

  saveUser: function (user) {
    this.user = user
    httpService.setToken(this.user.access_token)

    localStorage.setItem('token', user.access_token)
    localStorage.setItem('user', JSON.stringify(user))
  },

  isLoggedIn: function () {
    return this.user && this.user.access_token
  },

  logout: function () {
    this.user = null
    httpService.setToken(null)

    localStorage.removeItem('token')
    localStorage.removeItem('user')
  }
}
