export default {
  data () {
    return {}
  },
  methods: {
    confirmDelete: function () {
      return this.$confirm('This will permanently delete the item. Continue?', 'Warning', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        type: 'warning',
        center: true
      })
    },
    showError: function (message) {
      this.$notify.error({
        title: 'Error',
        message: message
      })
    },
    showSuccess: function (message) {
      this.$notify({
        title: 'Success',
        message: message,
        type: 'success'
      })
    },
    redirectToWebsite: function () {
      let host = window.location.host
      let protocol = window.location.protocol
      host = host.split('.')
      let env = host[host.length - 1]

      window.location.href = protocol + '//www.coinigniter.' + env
    }
  }
}
