// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import App from './App'

import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'

import Navbar from '@/components/Navbar'
import SideNav from '@/components/Sidenav'

import ToEtherFilter from '@/filters/toEther'
import ToDateFilter from '@/filters/toDate'
import ToNumberFilter from '@/filters/toNumber'

import mixin from '@/shared/mixins'

import '@/scss/theme.scss'

Vue.use(ElementUI, {locale})

Vue.component('navbar', Navbar)
Vue.component('sidenav', SideNav)

Vue.filter('toEther', ToEtherFilter)
Vue.filter('toDate', ToDateFilter)
Vue.filter('toNumber', ToNumberFilter)

Vue.mixin(mixin)

Vue.config.productionTip = false

window.bus = new Vue()

/* eslint-disable no-new */
window.vue = new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
