import Vue from 'vue'
import Router from 'vue-router'

import AuthPage from '@/pages/Auth'
import SignupPage from '@/pages/auth/Signup'
import SigninPage from '@/pages/auth/Signin'

import DashboardPage from '@/pages/Dashboard'
import IndexPage from '@/pages/dashboard/Index'
import InfoPage from '@/pages/dashboard/Info'
import AddressesPage from '@/pages/dashboard/Addresses'
import ApplicationsPage from '@/pages/dashboard/Applications'
import TransferPage from '@/pages/dashboard/Transfer'
import InteractPage from '@/pages/dashboard/Interact'
import DeployPage from '@/pages/dashboard/Deploy'
import TokensPage from '@/pages/dashboard/Tokens'
import SettingPage from '@/pages/dashboard/Setting'
import ProfilePage from '@/pages/dashboard/Profile'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/auth',
      name: 'AuthPage',
      component: AuthPage,
      redirect: '/auth/register',
      children: [
        {
          path: 'register',
          name: 'SignupPage',
          component: SignupPage
        },
        {
          path: 'login',
          name: 'SigninPage',
          component: SigninPage
        }
      ]
    },
    {
      path: '/dashboard',
      name: 'DashboardPage',
      component: DashboardPage,
      // redirect: '/info',
      children: [
        {
          path: 'index',
          name: 'IndexPage',
          component: IndexPage
        },
        {
          path: 'info',
          name: 'InfoPage',
          component: InfoPage
        },
        {
          path: 'addresses',
          name: 'AddressesPage',
          component: AddressesPage
        },
        {
          path: 'applications',
          name: 'ApplicationsPage',
          component: ApplicationsPage
        },
        {
          path: 'transfer',
          name: 'TransferPage',
          component: TransferPage
        },
        {
          path: 'interact',
          name: 'InteractPage',
          component: InteractPage
        },
        {
          path: 'deploy',
          name: 'DeployPage',
          component: DeployPage
        },
        {
          path: 'profile',
          name: 'ProfilePage',
          component: ProfilePage
        },
        {
          path: 'tokens',
          name: 'TokensPage',
          component: TokensPage
        },
        {
          path: 'setting',
          name: 'SettingPage',
          component: SettingPage
        }
      ]
    }
  ]
})
