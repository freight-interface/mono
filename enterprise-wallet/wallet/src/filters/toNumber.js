var numeral = require('numeral')
export default (number) => {
  return number ? numeral(number).format('0,0') : 0
}
