'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_URL: '"http://localhost:8081"',
  ETHERSCAN_API: '"https://api-rinkeby.etherscan.io/api"',
  GAS_API: '"https://ethgasstation.info/json/ethgasAPI.json"',
  NET_API: '"https://www.etherchain.org/api/basic_stats"'
})
