'use strict'
module.exports = {
  NODE_ENV: '"production"',
  API_URL: '"http://18.191.55.189"',
  ETHERSCAN_API: '"https://api-rinkeby.etherscan.io/api"',
  GAS_API: '"https://ethgasstation.info/json/ethgasAPI.json"',
  NET_API: '"https://www.etherchain.org/api/basic_stats"'
}
