const fs = require("fs");
const { app, ipcMain, ipcRenderer, BrowserWindow } = require('electron');
const path = require('path');

var data_path = app.getPath('documents') + '/enterprise-wallet';

if (!fs.existsSync(data_path)){
    fs.mkdirSync(data_path);
}


function writeFile (filename, content) {
	console.log(path.join(data_path, filename))
	fs.writeFile(path.join(data_path, filename), content, (error) => {
		if (error) {
			console.log("Error saving file", error)
			return
		}
	})
}

function readFile (filename,) {
	return new Promise((resolve, reject) => {	
		fs.readFile(path.join(data_path, filename), 'utf-8', (error, data) => {
			if (error) {
				console.log("Error reading file", error)
				reject(error)
				return
			}
			resolve(data)
		})
	})
}

const FSHelper = () => {
	console.log('APP PATH', data_path)
	ipcMain.on('save-data', (event, args) => {
		console.log(' i am here', args)
		let filename = args.filename + '.json'
		writeFile(filename, args.content)
	})

	ipcMain.on('read-data', (event, args) => {
		console.log(' i am here read data', args)
		let filename = args.filename + '.json'
		readFile(filename).then((data) => {
			event.sender.send('after-read-data', data)
		})
	})
}

module.exports = {
	FSHelper
}